package com.npe.dto.kafka.in.faq;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FaqQuestionDto {
    private String number;
    private String text;
    private String answer;
    private Integer categoryNumber;
}
