package com.npe.dto.kafka.in.cardOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CardUserInfo {
    private String fio;
    private String phone;
    private String email;
}
