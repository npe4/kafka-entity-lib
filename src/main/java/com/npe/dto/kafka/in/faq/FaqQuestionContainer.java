package com.npe.dto.kafka.in.faq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FaqQuestionContainer {
    private List<FaqQuestionDto> questions = new ArrayList<>();
}
