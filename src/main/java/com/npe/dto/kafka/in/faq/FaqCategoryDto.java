package com.npe.dto.kafka.in.faq;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FaqCategoryDto {
    private String id;
    private String text;
    private Integer categoryNumber;
}
