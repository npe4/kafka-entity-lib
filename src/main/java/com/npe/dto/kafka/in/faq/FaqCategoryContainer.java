package com.npe.dto.kafka.in.faq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FaqCategoryContainer {
    private List<FaqCategoryDto> categories;
}
