package com.npe.dto.kafka;

public enum TargetType {
    BOT, BANK
}
