package com.npe.dto.kafka;

public enum EventType {
    NOTIFICATION,
    FAQ_CATEGORY_REQUEST,
    FAQ_CATEGORY_REPLY,
    FAQ_QUESTION_REQUEST,
    FAQ_QUESTION_REPLY
}
