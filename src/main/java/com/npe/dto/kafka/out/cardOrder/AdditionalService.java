package com.npe.dto.kafka.out.cardOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdditionalService {
    private Integer number;
    private String name;
    private Double price;
}
