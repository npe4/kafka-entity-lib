package com.npe.dto.kafka.out.cardOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CardDocumentContainer {
    private List<CardDocument> documents;
}
