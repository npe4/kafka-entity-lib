package com.npe.dto.kafka.out.cardOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CardDocument {
    private String guid;
    private String filename;
}
