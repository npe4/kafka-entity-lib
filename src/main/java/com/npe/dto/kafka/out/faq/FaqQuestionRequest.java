package com.npe.dto.kafka.out.faq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FaqQuestionRequest {
    private Integer categoryNumber;
}
