package com.npe.dto.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageWrapper<T> {
    private String customerContact;
    private SystemType systemType;
    private TargetType targetType;
    private EventType eventType;
    private String correlationId;
    private Optional<T> payload;
}
